# CAPGI V2

## Structure du Projet

- `/docker` - Contient les Dockerfiles pour les services PHP, Node.js, et Cypress, ainsi que la configuration Nginx.
- `docker-compose.yml` - Définit les services, volumes et réseaux utilisés par Docker.
- `/public` - Dossier public de Symfony.
- `/src` - Code source de l'application Symfony.
- `assets.config.json` - Configuration pour AssetsMapper.
- `Makefile` - Commandes pour faciliter le démarrage des containers et la gestion de Symfony.

## Services Docker

- `php` - Container PHP avec les extensions nécessaires pour Symfony.
- `postgres` - Base de données PostgreSQL.
- `node` - Environnement Node.js pour la gestion des assets.
- `cypress` - Environnement Cypress pour les tests E2E.
- `nginx` - Serveur web Nginx configuré pour servir l'application Symfony.

## Installation

1. Clonez le dépôt :
   ```bash
   git clone [url-du-dépôt]
   cd [nom-du-dossier]
   ```

2. Construisez et lancez les containers Docker :
   ```bash
   make up
   ```

3. Installez les dépendances de Symfony :
   ```bash
   make composer-install
   ```

4. Créez la base de données et appliquez les migrations :
   ```bash
   make doctrine-create
   make doctrine-migrate
   ```

## Gestion des Assets avec TailwindCSS

### Installation de TailwindCSS

1. TainwindCss est pré installé sur le projet.

## Utilisation

- Accédez à l'application via `http://localhost:9000`.
- Utilisez `make` pour exécuter les commandes définies dans le `Makefile`.

## Tests

- Exécutez les tests Cypress :
  ```bash
  make cypress-tests
  ```

## Notes

- Assurez-vous de configurer correctement les variables d'environnement et les paramètres spécifiques à votre environnement de production.

## Support

Pour toute question ou problème, veuillez ouvrir un ticket dans le système de suivi des problèmes du dépôt.
