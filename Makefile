.PHONY:

up:
	docker compose up -d

down:
	docker compose down

install-packages:
	docker compose run --rm node npm install

watch-assets:
	docker compose run --rm php bin/console tailwind:build --watch

cypress-tests:
	docker compose run --rm cypress npx cypress open

doctrine-create:
	docker compose run --rm php bin/console d:d:c --if-not-exist

doctrine-migration:
	docker compose run --rm php bin/console make:migration

doctrine-migrate:
	docker compose run --rm php bin/console d:m:m -n